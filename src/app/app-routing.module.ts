import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AuthGuard } from './components/auth/authGuard';
import { HomeLayoutComponent } from './components/layout/home-layout/home-layout.component';
import { LoginLayoutComponent } from './components/layout/login-layout/login-layout.component';
import { ViewProductComponent } from './components/products/view-product/view-product.component';
import { ReportsComponent } from './components/reports/reports.component';
import { TransactionComponent } from './components/transaction/transaction.component';
import { UsersDataComponent } from './components/users-data/users-data.component';
import { ReferalsComponent } from './components/referals/referals.component';
import { BillingComponent } from './components/billing/billing.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
const routes: Routes = [
  {
    path: '',
    component: HomeLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path: 'products',
        component: ViewProductComponent,
      },
      {
        path: 'reports',
        component: ReportsComponent,
      },
      {
        path: 'transaction',
        component: TransactionComponent,
      },
      {
        path: 'users',
        component: UsersDataComponent,
      },
      {
        path: 'referals',
        component: ReferalsComponent,
      },
      {
        path: 'billing',
        component: BillingComponent
      }, 
      {
        path: 'upload',
        component: FileUploadComponent
      }
    ],
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
      },
    ],
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
