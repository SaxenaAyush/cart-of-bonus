import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort} from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/bonus.service';
import { FileDownloadService } from 'src/app/services/file-download.service';

@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.scss'],
})
export class BillingComponent {
  displayedColumns: string[] = [
    'user_id',
    'parent_id',
    'txn_date',
    'txn_id',
    'status',
    'total_debit',
    'margin_debit',
    'NUAF_debit',
    'total_credit', 
    'MVP_credit',
    'referral_credit',
    'community_credit',
    'type',
    'is_flag',
  ];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  filterList = ['DEBIT', 'CREDIT'];
  filterType!: '';
  public referalsList: any;
  itemsPerPage: number = 25;
  currentPage: number = 1;
  totalItems: number = 100;
  inputValue: string = '';
  fromDate: string = '';
  toDate: string = '';
  transType!: string;
  reason!: string;
  selected = 'option2';
  id!: number;
  dataSource!: MatTableDataSource<any>;
  pageSizeOptions: number[] = [25, 50, 100];
  element: any;
  constructor(
    private apiService: ApiService,
    private spinner: NgxSpinnerService,
    private fileService: FileDownloadService
  ) {}

  public ngOnInit(): void {
    this.getBilling();
  }

  getBilling() {
    const req = {
      page: this.currentPage,
      limit: this.itemsPerPage,
      search: this.inputValue,
      from_date: this.toDate,
      to_date: this.fromDate,
      type: this.filterType,
    };
    this.spinner.show();
    this.apiService.makeApiRequest('POST', 'billing', req).subscribe((res) => {
      if (res) {
        this.spinner.hide();
        this.dataSource = new MatTableDataSource(res.data.result);
        // this.itemsPerPage = this.pageSizeOptions[0];
        this.dataSource.sort = this.sort;
        this.totalItems = res.data.total;
      }
    });
  }

  onPageChange(event: any) {
    this.currentPage = event.pageIndex + 1;
    this.itemsPerPage = event.pageSize;
    this.getBilling();
  }

  applyFilter() {
    this.currentPage = 1;
    this.getBilling();
    this.paginator.pageIndex = 0;
  }
  getelementID(ID: number) {
    this.id = ID;
  }

  downloadFile(): void {
    const req = {
      from_date: this.fromDate,
      to_date: this.toDate,
    };
    this.spinner.show();
    this.apiService
      .makeApiRequest('POST', 'downloadBilling', req)
      .subscribe((res) => {
        if (res) {
          this.spinner.hide();
          this.fileService.downloadFile(res.message, 'BillingFile');
        }
      });
  }

  raiseFlag(): void {
    const req = {
      id: this.id,
      resason: this.reason,
    };
    this.spinner.show();
    this.apiService
      .makeApiRequest('POST', 'raiseFlag', req)
      .subscribe((res) => {
        if (res) {
          this.spinner.hide();
          this.getBilling();
        }
      });
  }
  onSortChange(event: Sort): void {
    if (event.active === 'id') {
      const sortDirection = event.direction === 'asc' ? 'desc' : 'asc';
      const sortColumn = event.active;
console.log(sortColumn)
      const sortString = sortDirection;
      const req = {
        page: this.currentPage,
        limit: this.itemsPerPage,
        search: this.inputValue,
        parentSort: sortString
      };
      this.spinner.show();
      this.apiService.makeApiRequest('POST', 'billing', req).subscribe((res) => {
        if (res) {
          this.spinner.hide();
          this.dataSource = res.data.result;
          this.totalItems = res.data.totalItems;
        }
      });
    }
  }
  clearFilter(){
    this.currentPage = 1;
    this.inputValue= '';
    this.toDate= '';
    this.fromDate = '';
    this.filterType='';
    this.getBilling();
  }
}
