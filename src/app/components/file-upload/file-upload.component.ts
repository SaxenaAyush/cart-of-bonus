import { Component } from '@angular/core';
import { NgxSpinnerService, Spinner } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { FileUploadService } from 'src/app/services/file-upload.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
})
export class FileUploadComponent {
  selectedFile: File | null = null;

  constructor(private fileUploadService: FileUploadService,
    private toaster: ToastrService,    private spinner: NgxSpinnerService,
    ) { }

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
    this.onUpload();
  }

  onUpload() {
    this.toaster.success('dsfsfsfsdf')
    if (this.selectedFile) {
      this.spinner.show();
      this.fileUploadService.uploadFile(this.selectedFile)
        .subscribe((response) => {
          debugger;
          console.log(response);
          if(response){
            this.spinner.hide();
            this.toaster.success('file upload successfully')
          }
        });
    }
  } 
}
