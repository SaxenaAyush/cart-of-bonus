import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/bonus.service';
@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.scss'],
})
export class ViewProductComponent implements OnInit {
  displayedColumns: string[] = [
    'product_id',
    'name',
    'price',
    'offer_price',
    'status',
    'margin',
    'mvp_points',
  ];
  Food = [
    { value: 'steak-0', viewValue: 'Steak' },
    { value: 'pizza-1', viewValue: 'Pizza' },
    { value: 'tacos-2', viewValue: 'Tacos' },
  ];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  dataSource!: MatTableDataSource<any>;

  public productList: any;
  itemsPerPage: number = 25;
  currentPage: number = 1;
  totalItems: number = 100;
  inputValue: string = '';
  toDate: string = '';
  fromDate: string = '';
  pageSizeOptions: number[] = [25, 50, 100];
  constructor(private apiService: ApiService,private spinner: NgxSpinnerService,
    private toaster: ToastrService) {}

  public ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    const req = {
      page: this.currentPage,
      limit: this.itemsPerPage,
      search: this.inputValue,
      from_date: this.fromDate,
      to_date: this.toDate,
    };
    this.spinner.show();
    this.apiService.makeApiRequest('POST', 'products', req).subscribe((res) => {
      if(res){
        this.spinner.hide();
        this.dataSource = res.data.products;
        this.totalItems = res.data.totalItems;    
      }
    });
  }

  onPageChange(event: any) {
    this.currentPage = event.pageIndex + 1;
    this.itemsPerPage = event.pageSize;
    this.getProducts();
  }

  applyFilter() {
    this.currentPage = 1;
    this.paginator.pageIndex = 0;
    this.getProducts();
  }

  onSortChange(event: Sort): void {
    if (event.active === 'product_id') {
      const sortDirection = event.direction === 'asc' ? 'desc' : 'asc';

      const sortString = sortDirection;
      const req = {
        page: this.currentPage,
        limit: this.itemsPerPage,
        search: this.inputValue,
        from_date: this.fromDate,
        to_date: this.toDate,
        parentSort: sortString
      };
      this.spinner.show();
      this.apiService.makeApiRequest('POST', 'products', req).subscribe((res) => {
        if (res) {
          this.spinner.hide();
          this.dataSource = res.data.products;
          this.totalItems = res.data.totalItems;
        }
      });
    }
  }
}
