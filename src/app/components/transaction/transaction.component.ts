import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/services/bonus.service';
import { FileDownloadService } from 'src/app/services/file-download.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss'],
})
export class TransactionComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  dataSource!: MatTableDataSource<any>;

  displayedColumns: string[] = [
    'user_id',
    'purchase_value',
    'points_earned',
    'order_id',
    'txn_date',
    'payment_type',
    'points_used',
  ];
  filterList = ['UPI', 'CARD', 'CASH', 'POINTS'];
  filterType!: '';
  public transactionList: any;
  itemsPerPage: number = 25;
  currentPage: number = 1;
  totalItems: number = 100;
  inputValue!: string;
  toDate: string = '';
  fromDate: string = '';
  pageSizeOptions: number[] = [25, 50, 100];
  constructor(
    private apiService: ApiService,
    private spinner: NgxSpinnerService,
    private fileService: FileDownloadService
  ) { }

  public ngOnInit(): void {
    this.getTransaction();
  }

  getTransaction() {
    const req = {
      userId: this.inputValue,
      page: this.currentPage,
      limit: this.itemsPerPage,
      from_date: this.fromDate,
      to_date: this.toDate,
      paymentType: this.filterType,
    };
    this.spinner.show();
    this.apiService
      .makeApiRequest('POST', 'transactions', req)
      .subscribe((res) => {
        if (res) {
          this.spinner.hide();
          this.dataSource = new MatTableDataSource(res.data.transactions);
          this.dataSource.sort = this.sort;
          this.totalItems = res.data.totalItems;
        }
      });
  }

  onPageChange(event: any) {
    this.currentPage = event.pageIndex + 1;
    this.itemsPerPage = event.pageSize;
    this.paginator.pageIndex = 0;
    this.getTransaction();
  }

  applyFilter() {
    this.currentPage = 1;
    this.getTransaction();
  }
  clearFilter() {
    this.currentPage = 1;
    this.getTransaction();
    this.inputValue = '';
    this.toDate = '';
    this.fromDate = '';
    this.filterType = '';
    this.getTransaction();
  }
  downloadFile(): void {
    const req = {
      from_date: this.fromDate,
      to_date: this.toDate,
    };
    this.spinner.show();
    this.apiService
      .makeApiRequest('POST', 'downloadTransaction', req)
      .subscribe((res) => {
        if (res) {
          this.spinner.hide();
          this.fileService.downloadFile(res.message, 'TransactionFile');
        }
      });
  }

  onSortChange(event: Sort): void {
    debugger;
    if (event.active === 'id') {
      const sortDirection = event.direction === 'asc' ? 'desc' : 'asc';

      const sortString = sortDirection;
      const req = {
        page: this.currentPage,
        limit: this.itemsPerPage,
        from_date: this.fromDate,
        to_date: this.toDate,
        paymentType: this.filterType,
        parentSort: sortString, 
        userId: this.inputValue,

      };
      this.spinner.show();
      this.apiService.makeApiRequest('POST', 'transactions', req).subscribe((res) => {
        if (res) {
          this.spinner.hide();
          this.dataSource = new MatTableDataSource(res.data.transactions);
          this.dataSource.sort = this.sort;
          this.totalItems = res.data.totalItems;
        }
      });
    }
  }
}
